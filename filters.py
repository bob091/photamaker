from PyQt5.QtGui import QPixmap
from PIL import Image
import numpy as np
from scipy import ndimage, misc
import matplotlib.pyplot as plt

class Filters():
    """Klasa w której są zaimportowane agorytmy fitrów"""
    def medianFiler(self, fileroot, mask=5):
        """Fitr medianowy"""
        try:
            im = Image.open(fileroot)
            width, height = im.size
            result = ndimage.median_filter(im, size=mask) # size stopień rozmycia(maska)
            image = Image.frombuffer('RGB', (width, height), result, "raw", 'RGB', 0, 1)
            return image
        except:
            print("ERROR")
