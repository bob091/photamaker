from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtWidgets import QLabel, QGridLayout, QPushButton
import PyQt5.QtGui
from PIL import Image

from tkinter import filedialog

import filters


class PhotoMaker(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.mask = 5  # Wartość domyślan przekazana do funkcji w przypadaku nie podania maski

        self.interfejs()

    def interfejs(self):

        # etykiety
        self.label1 = QLabel("Obrazek wczytany", self)
        self.label2 = QLabel("Wynik", self)

        # przypisanie widgetów do układu tabelarycznego
        ukladT = QGridLayout()
        ukladT.addWidget(self.label1, 0, 0)
        ukladT.addWidget(self.label2, 0, 1)

        # przyciski
        loadButton = QPushButton("&Wczytaj plik (*jpg)", self)
        makeButton = QPushButton("&Fitruj medianą", self)
        saveButton = QPushButton("&Zapisz", self)
        maskButton = QPushButton("&Maska", self)

        ukladT.addWidget(loadButton, 1, 0)
        ukladT.addWidget(makeButton, 1, 1)
        ukladT.addWidget(saveButton, 2, 0)
        ukladT.addWidget(maskButton, 2, 1)

        # przypisanie utworzonego układu do okna
        self.setLayout(ukladT)
        loadButton.clicked.connect(self._operation)
        makeButton.clicked.connect(self._operation)
        saveButton.clicked.connect(self._operation)
        maskButton.clicked.connect(self._operation)

        self.setGeometry(200, 200, 1000, 500)
        self.setWindowIcon(QIcon('image/photoLogo.png')) # ikona
        self.setWindowTitle("Photo Maker")
        self.show()

    def _operation(self):
        """Rozpoznowanie jaka operacja ma być wykonana po naciśnięciu przycisku"""

        sender = self.sender()

        if sender.text() == "&Wczytaj plik (*jpg)":
            self._loadPicture()
        elif sender.text() == "&Fitruj medianą":
            try:
                print(self.mask)
                img = filters.Filters().medianFiler(self.fileroot, self.mask)
                img.save("temp/temp.jpg")
                self.label2.setPixmap(QPixmap("temp/temp.jpg"))
                self.label2.show()
            except:
                self.label2.setText("Niemożna wyświetlić obrazka")
        elif sender.text() == "&Maska":
           self.mask = self._getIntegerToMask()
        elif sender.text() == "&Zapisz":
            self._savePicture()


    def _loadPicture(self):
        """Wczytanie obrazu"""
        tk = filedialog.Tk()    # Ukrywanie root dialog tkinter
        tk.withdraw()
        self.fileroot = filedialog.askopenfilename()

        try:
            self.label1.setPixmap(QPixmap(self.fileroot))
            self.label1.show()
        except:
            self.label1.setText("Nie wczytano obrazu")

    def _getIntegerToMask(self):
        """Oknie dialogowe do podawania maski"""
        i, okPressed = QInputDialog.getInt(self, "Podaj Maskę", "Maska:", 5, 0, 100, 1)
        if okPressed:
            return i
    def _savePicture(self):
        """Zapisywania finalego obrazu na komputerze"""
        tk = filedialog.Tk()  # Ukrywanie root dialog tkinter
        tk.withdraw()
        filename = filedialog.asksaveasfilename(filetypes=[("Plik graficzny(*.jpg)" ,"*.jpg")], defaultextension = "*.jpg")
        fileIm = Image.open("temp/temp.jpg")    # Wczytanie Pliku temp
        if filename:
            fileIm = fileIm.save(filename) # Zapis pliku jpg we wskazanym miejscu

if __name__ == '__main__':
    import sys

    app = QApplication(sys.argv)
    window = PhotoMaker()
    sys.exit(app.exec_())

